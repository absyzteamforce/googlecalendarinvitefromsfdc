public class InterviewerInvitation {
   @Future(callout=true)
    public static void Interviewermail(){

String interviewer='Mail receiver';
       String resumeurl=inv.Candidate__r.Link_to_Resume_in_Google_Drive__c;//alternate link from google drive
      String resume='Resume of  '+inv.Candidate__r.Name;
           
            String Summary2='Title of the event';    
                       String description='description of invitation goes here';

            
            String btime=string.valueOf(inv.Interviewed_Time__c);//interview time
        string atime2=string.valueOf(inv.Interviewed_Date__c);
       // String etime=string.valueOf(EndTime);
       System.debug('start time'+btime);
        System.debug('date is'+inv.Interviewed_Date__c);
        String time1=btime.substring(0,5);
         // "2017-12-20T14:30:00+05:30"'
       
        String endattime='';
       //the time format received is "6:30PM" which was converted down to required format"2017-12-20T14:30:00+05:30"
    String che=btime.substring(6,7);
        System.debug('am or pm'+che);
         if(che=='P'||che=='p'){
            Integer a=Integer.valueOf(btime.substring(0,2));
            a=a+12;
            String b=String.valueOf(a);
            System.debug('hours'+b);
             time1=b+btime.substring(2,5);
             System.debug('pm time is'+time1);
        }
          String stime=atime2.substring(0,10)+'T'+time1+':00'+'+05:30';
        System.debug('start time is'+stime); //2017-12-18 00:00:00T08:30 PM:00+05:30
      Integer et=Integer.valueOf(stime.substring(11, 13))+1;
        String etime=stime.substring(0,11)+et+stime.substring(13);
        System.debug('end time is'+etime);
        System.debug('needed format is 2017-12-20T14:30:00+05:30');
    
//code for renewing access token with refresh token            
            Integer customchecker=0;
            String accesstoken;
            String errorMessage ='';
            String  consumerKey='199214975867-ruti2f4jhjn279arc8reolfashmvvl4i.apps.googleusercontent.com';
   		    String clientSecret='4aMYbfSHYkThluO-wnitYcGK'; 
            Http http = new Http();
            HttpRequest httpReq = new HttpRequest();
            HttpResponse httpRes = new HttpResponse();
            httpReq.setEndpoint('https://www.googleapis.com/oauth2/v4/token');
            httpReq.setMethod('POST');
            httpReq.setHeader('Content-Type', 'application/x-www-form-urlencoded');
            
            List<GoogleCalenderInfo__c> googleSettingInfoList = new List<GoogleCalenderInfo__c>();
            googleSettingInfoList = GoogleCalenderInfo__c.getAll().values();
            String refreshToken;
        
            refreshToken='1/WOJfzGHYnGfJiXB7xNQDx3U1AbOVxQrLdB28xQPSvB_iXx73epdBN3lehVZWZ9UN';

            System.debug('#### refreshToken '+refreshToken);
            String refreshTokenBody = 'client_id='+consumerKey+'&client_secret='+clientSecret+'&refresh_token='+refreshToken
                                            +'&grant_type=refresh_token';
            System.debug('#### refreshTokenBody '+refreshTokenBody);
            
            httpReq.setBody(refreshTokenBody);
                    
            try{
               httpRes = http.send(httpReq); 
                System.debug('response is'+httpRes.getBody());

                if(httpRes.getStatusCode() == 200){
                    System.debug('inside 200status of renewing');
                    Map<String,object> TokenInfo = (Map<String,object>)JSON.deserializeUntyped(httpRes.getBody());
                    GoogleCalenderInfo__c googleSettingInfo = googleSettingInfoList[0];
                    googleSettingInfo.Access_Token__c = String.valueOf(TokenInfo.get('access_token'));
                    accesstoken=String.valueOf(TokenInfo.get('access_token'));
                    System.debug('do Refresh Token '+googleSettingInfo);
    //code for renewing access token ends here.
                    if((accesstoken!=null)){
                        //Invitation code
                    if(inv.Interview_Round__c!=null){
                        
//code for firing calender event begins here      
                          String createEventEndPoint = 'https://www.googleapis.com/calendar/v3/calendars/primary/events?sendNotifications=true&supportsAttachments=true';
                          String attacs='';
                          System.debug('link resume'+resumeurl);
                          String attes='{'+'"email":'+'"'+interviewer+'"'+'}';
                          attacs=attacs+'{'+'"fileUrl":'+'"'+resumeurl+'"'+','+'"title":'+'"'+resume+'"'+'}';
                           String createEventBody = '{' +
'"attendees": ['+attes+'],'+
'"attachments": ['+attacs+'],'+
'"end": {'+
'"dateTime": '+'"'+etime+'"'+
'},'+
'"reminders": {'+
'"useDefault": true'+
'},'+
'"start": {'+
'"dateTime": '+'"'+stime+'"'+
'},'+
'"summary":'+'"'+Summary2+'"'+','+
'"description":'+'"'+descrip+'"'+','+
'"location":'+'"'+inv.Location__c+'"'+
'}';
System.debug('#### createEventBody '+createEventBody );
Http http2 = new Http();
HttpRequest httpReq2 = new HttpRequest();
HttpResponse HttpRes2 = new HttpResponse();

httpReq2.setEndpoint(createEventEndPoint);
httpReq2.setMethod('POST');
httpReq2.setBody(createEventBody );
httpReq2.setHeader('Content-Type', 'application/json');
httpReq2.setHeader('Authorization','Bearer '+accessToken);
try{
HttpRes2 = http2.send(httpReq2);
if(HttpRes2.getStatusCode() == 200){
system.debug('Calendar Event Successfully '+'\n'+' '+
HttpRes2.getBody()+'\n');
}else{
String errorMessage2 = 'Unexpected Error while communicating with Google Calendar API. '+'Status '+HttpRes2.getStatus()+' and Status Code '+HttpRes2.getStatuscode();
System.debug(errorMessage2);
}
}catch(System.Exception e1){
System.debug('#### Exception Executed : '+e1.getMessage() + ' '+e1.getStackTraceString() + ' '+e1.getLineNumber());
}
//code for firing calender event ends here
}
                        
                        
                   
 
                    else{
  }
              
                        }
                }}
            catch(System.Exception e){
                
                System.debug('#### Exception Executed '+e.getStackTraceString() + ' '+e.getMessage());
                if(String.valueOf(e.getMessage()).startsWith('Unauthorized endpoint')){
                    errorMessage = 'Unauthorize endpoint: An Administer must go to Setup -> Administer -> Security Control ->'
                        +' Remote Site Setting and add '+' '+ 'https://www.googleapis.com/oauth2/v4/token' +' Endpoint';
                }
                else{
                    errorMessage = 'Unexpected Error while communicating with Google Calendar API. '
                        +'Status '+httpRes.getStatus()+' and Status Code '+httpRes.getStatuscode();
                }
            }
       
   

        }
  
    
    }
    